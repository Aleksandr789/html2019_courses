//Fixed header
$(window).scroll(function() {
  if ($(this).scrollTop() > 600) {
    $('.header').addClass('fixed');
    $('.header')
      .find('a')
      .css('color', '#2f2933');
    $('.header')
      .find('.menu-item')
      .hover(
        function() {
          $(this).css('border', '2px solid #2f2933');
        },
        function() {
          $(this).css('border', '2px solid transparent');
        },
      );
  } else {
    $('.header').removeClass('fixed');
    $('.header')
      .find('a')
      .css('color', '#ffffff');
    $('.header')
      .find('.menu-item')
      .hover(
        function() {
          $(this).css('border', '2px solid #ffffff');
        },
        function() {
          $(this).css('border', '2px solid transparent');
        },
      );
  }
});

//Dropdown active
$('.profile-box').on({
  mouseenter: function(){
    if($('.navbar-responsive:visible').length) {
      return;
    }
    $('.dropdown-content').show(400);

  },
   mouseleave: function(){
    $('.dropdown-content').hide(400);
  }
});

//Scroll to section
$('.header__menu')
  .find('a')
  .click(function(e) {
    e.preventDefault();
    var section = $(this).attr('href');
    console.log(section);

    if (section === '#' || section === '#Home') {
      $('html, body').animate({ scrollTop: 0 }, 600);
    } else {
      $('html, body').animate({
        scrollTop: $(section).offset().top - 30,
      });
    }
  });
