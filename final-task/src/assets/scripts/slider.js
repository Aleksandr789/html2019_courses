require('slick-carousel');
require('slick-carousel/slick/slick.css');
require('slick-carousel/slick/slick-theme.css');
const $ = require('jquery');

$('.slick').each(function(){
	var $this = $(this),
		$blockArrows = $('.controls-arrows', $this),
		$blockDots = $('.controls-dots', $this),
		$slick = $('.slider', $this);
	$slick.slick({
		dots: true,
		arrows: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		appendArrows: $blockArrows,
		appendDots: $blockDots
    });

    //Hide the Previous button.
 $('.slick-prev').hide();
});
